#!/bin/sh

PATH=/usr/bin
TMUX=/usr/bin/tmux
SESSION_NAME="$(basename $0 .sh)"
RET=0

echo -n "checking that ${TMUX} can start a new session with 'top'..."

${TMUX} new-session -d -s ${SESSION_NAME} 'top' || ( echo "FAIL" ; exit 1 )

TOPPID=$(pgrep -x top)
PGREPCODE=$?
if [ -z "${TOPPID}" ] || [ ${PGREPCODE} -ne 0 ]; then
    RET=1
elif [ ! -d /proc/${TOPPID} ]; then
    RET=1
fi

${TMUX} kill-session -t ${SESSION_NAME} || ( echo "FAIL" ; exit 1 )

[ ${RET} -eq 0 ] && echo "ok" || echo "FAIL"
exit ${RET}
